# Userstyles
A repository of my userstyles.

## Userstyles List
This list will be immediately updated once I publish a new one :D

- MaterialDDG ([Install](https://userstyles.world/api/style/1324.user.css)): A Material Design-inspired UserCSS for DuckDuckGo.
- Messenger Dark ([Install](https://userstyles.world/api/style/1325.user.css)): A dark theme for Facebook Messenger.
- Messenger Dynamic Sidebar ([Install](https://userstyles.world/api/style/1335.user.css)): Show and hide your chat sidebar seamlessly.
- Old Teddit Dark ([Install](https://userstyles.world/api/style/1579.user.css)): A dark theme for teddit with the look and feel of Old Reddit and RES night mode.

## Installation
Before installing my userstyles, make sure you have [Stylus](https://add0n.com/stylus.html) extension installed.

Most of my userstyles are hosted on [UserStyles.world](https://userstyles.world).

My profile: <https://userstyles.world/user/reizumi>
